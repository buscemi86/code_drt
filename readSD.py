import matplotlib.pyplot as plt
import utils.event as evn

#read a zip file containing many json files, and convert all the json files in a collection of SdEvent object
eventColl_all = evn.SdEventCollection("data/SD/SD.zip")


#read the collection
events=eventColl_all.get_collection()
#select a single event of the collection
my_event=events[10]

##methods to plot the pmts signal of an SdEvent
my_event.plot()  

#to plot signals of a single station use the function: plotStation(id) , where id is the id of the station
my_event.plotStation(631)

#get a datafreme
df=eventColl_all.get_df()

#create an object that collect the attributes of each SdEvent in lists of attrubute
varlist=eventColl_all.get_varList()
#each list of attributes can be easily plotted
plt.hist(varlist.energy)   
plt.show()


#event=evn.readJson('data/SD/70767063900')
#event.plot() 
#print(event.augerId)
