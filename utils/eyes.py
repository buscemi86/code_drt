import pandas as pd


class eyes:
	def __init__(self, data):
		self.eyes = eye(pd.DataFrame(data)) 
		


	def get_eyes(self):
	  return self.eyes
	  
	  	  
	def __repr__(self):
    	  return  "<pandas.DataFrame> \n"
    	

class eye:
    def __init__(self, filename):
        self.data = 		filename
        self.eyeId		= self.data['Eye']
        self.eyeName		= self.data['eyeName']
        self.gpstime   	= self.data['gpstime']
        self.gpsnanotime   	= self.data['gpsnanotime']
        self.theta	   	= self.data['theta']
        self.thetaErr   	= self.data['thetaErr']
        self.phi	   	= self.data['phi']
        self.phiErr   		= self.data['phiErr']
        self.l              =self.data['l']
        self.b              =self.data['b']
        self.ra              =self.data['ra']
        self.dec              =self.data['dec']
        self.TotalEnergy	= self.data['TotalEnergy']
        self.TotalEnergyErr	= self.data['TotalEnergyErr']
        self.CalEnergy   	= self.data['CalEnergy']
        self.CalEnergyErr	= self.data['CalEnergyErr']
        self.Xmax   		= self.data['Xmax']
        self.XmaxErr	   	= self.data['XmaxErr']
        self.HeightXmax   	= self.data['HeightXmax']
        self.DistXmax	   	= self.data['DistXmax']
        self.dEdXmax     	= self.data['dEdXmax']
        self.dEdXmaxError	= self.data['dEdXmaxError']
        self.X              = self.data['x']
        self.Y              = self.data['y']
        self.Z              = self.data['z']
        self.dX             = self.data['dx']
        self.dY             = self.data['dy']
        self.easting           = self.data['easting']
        self.northing          = self.data['northing']
        self.altitude          = self.data['altitude']
        self.CherenkovFraction = self.data['CherenkovFraction']
        self.minViewAngle      = self.data['minViewAngle']        
        self.UspL   		= self.data['UspL']
        self.UspLError	   	= self.data['UspLError']
        self.UspR   		= self.data['UspR']
        self.UspRError	   	= self.data['UspRError']
        self.FlagSpectrumEye   = self.data['FlagSpectrumEye']
        self.FlagCalibEye	    = self.data['FlagCalibEye']
        self.FlagXmaxEye    	= self.data['FlagXmaxEye']
        self.HottestStationId	= self.data['HottestStationId']
        self.DistSDPStation	    = self.data['DistSDPStation']
        self.DistAXISStation	= self.data['DistAXISStation']
        self.atmDepthProf   	= self.data['atmDepthProf']
        self.energyDepositProf  = self.data['energyDepositProf']
        self.energyDepositProfErr   = self.data['energyDepositProfErr']
        self.pixelID	   		= self.data['pixelID']
        self.pixelTime   		= self.data['pixelTime']
        self.pixelCharge	   	= self.data['pixelCharge']
        self.pixelStatus	   	= self.data['pixelStatus']
        
        


