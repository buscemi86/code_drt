import zipfile
import json
import pandas as pd
import utils.station as wcd
import utils.eyes as fd
import matplotlib.pyplot as plt
import numpy as np
import utils.map as auger_map 

class SdColl(list):
	def __init__(self):
		self =[]
	
	def add(self, event):
		self.append(event)
		
	def get(self):
		return self

	def __repr__(self):
		return "Collection of SdEvent\n"
		
	
class SDVariablesColl():
	def __init__(self):
		self.gpstime		= []
		self.gpsnanotime		= []
		self.augerId 		= []
		self.sdId   		= []
		self.code    		= []
		self.version   	= []
		self.theta    		= []
		self.phi		= []
		self.energy		= [] 
		self.dtheta   		= []
		self.dphi     		= []
		self.denergy  		= []
		self.l   		= []
		self.b   		= []
		self.ra  		= []
		self.dec 		= []
		self.x			= []
		self.y			= []
		self.z 		= []
		self.dx  		= []
		self.dy		= []
		self.easting			= []
		self.northing		= []
		self.altitude 		= []
		self.r   		= []
		self.dr  		= []
		self.s1000		= []
#		self.ldftype 		= []
		self.beta   		= []
		self.gamma  		= []
		self.chi2   		= []
		self.ndf   		= []
		self.nbstat		= []
		self.recstations	= []
		self.stations		= []
		
		
		

class SdEventCollection:
	def __init__(self, file_name,n_ev=0, ev_id=None):
		data_json=[]
		self.list=[]
		self.flag=ev_id
		if (ev_id==None):
			with zipfile.ZipFile(file_name,'r') as z:
				if(n_ev==0):
					n_ev=len(z.namelist())
				j=0
				for filename in z.namelist():
					if (j<n_ev):		
						self.list.append(filename)
						with z.open(filename) as f:
							data = f.read()
						data_json.append(data)
					j=j+1
			self.listSD=SdColl()
			for i in range(len(data_json)):	
				event=SdEvent(json.loads(data_json[i]))
				self.listSD.add(event)
			
		else:
			with zipfile.ZipFile(file_name,'r') as z:
				for filename in z.namelist():
					if(filename==ev_id):
						self.list.append(filename)
						with z.open(filename) as f:
							data = f.read()
						data_json.append(data)
			self.listSD=SdColl()
			for i in range(len(data_json)):	
				event=SdEvent(json.loads(data_json[i]))
				self.listSD.add(event)	
		
		self.varList=SDVariablesColl()
		for event in self.listSD:
			self.varList.gpstime.append(event.gpstime)
			self.varList.gpsnanotime.append(event.gpsnanotime)
			self.varList.augerId.append(event.augerId)
			self.varList.sdId.append(event.sdId)
			if(event.code=='Offline'):
			    code_index=1
			else:
			    code_index=0
			self.varList.code.append(code_index)
			self.varList.version.append(event.version)
			self.varList.theta.append(event.theta)
			self.varList.phi.append(event.phi)
			self.varList.energy.append(event.energy)
			self.varList.dtheta.append(event.dtheta)
			self.varList.dphi.append(event.dphi)
			self.varList.denergy.append(event.denergy)
			self.varList.l.append(event.l)
			self.varList.b.append(event.b)
			self.varList.ra.append(event.ra)
			self.varList.dec.append(event.dec)
			self.varList.x.append(event.x)
			self.varList.y.append(event.y)
			self.varList.z.append(event.z)
			self.varList.dx.append(event.dx)
			self.varList.dy.append(event.dy)
			self.varList.easting.append(event.easting)
			self.varList.northing.append(event.northing)
			self.varList.altitude.append(event.altitude)
			self.varList.r.append(event.r)
			self.varList.dr.append(event.dr)
			self.varList.s1000.append(event.s1000)
#			self.varList.ldftype.append(event.ldftype)
			self.varList.beta.append(event.beta)
			self.varList.gamma.append(event.gamma)
			self.varList.chi2.append(event.chi2)
			self.varList.ndf.append(event.ndf)
			self.varList.nbstat.append(event.nbstat)

		dict={
		'augerId':self.varList.augerId,
		'sdId':self.varList.sdId,
		'gpstime':self.varList.gpstime,
		'gpsnanotime':self.varList.gpsnanotime,
		'code':self.varList.code,
#		'version':self.varList.version,
		'theta':self.varList.theta,
		'phi':self.varList.phi,
		'energy':self.varList.energy,
		'dtheta':self.varList.dtheta,
		'dphi':self.varList.dphi,
		'denergy':self.varList.denergy,
		'l':self.varList.l,
		'b':self.varList.b,
		'ra':self.varList.ra,
		'dec':self.varList.dec,
		'x':self.varList.x,
		'y':self.varList.y,
		'z':self.varList.z,
		'dx':self.varList.dx,
		'dy':self.varList.dy,
		'easting':self.varList.easting,
		'northing':self.varList.northing,
		'altitude':self.varList.altitude,
		'r':self.varList.r,
		'dr':self.varList.dr,
		's1000':self.varList.s1000,
#		'ldftype':self.varList.ldftype,
		'beta':self.varList.beta,
		'gamma':self.varList.gamma,
		'chi2':self.varList.chi2,
		'ndf':self.varList.ndf,
		'nbstat':self.varList.nbstat}
		self.df=pd.DataFrame(dict)
	
   	
	def get_df(self):
   		return self.df




					
		
	def list(self):
		return self.list
		
	def get_collection(self):
		return self.listSD

	def get_event(self):
		if(self.flag==None):
			print("Warning: get_event() works only for single event selection")
			return 
		else:
			return self.listSD[0]
	def get_varList(self):
		return self.varList
	
	def __repr__(self):
   		return  "List of events in the zip file: View the list using SdEvent.list; access i^th element using SdEvent.list[i] \n"
    

class SdEvent:
    def __init__(self, filename):
        self.data = 		filename
        self.gpstime		= self.data['gpstime']
        self.gpsnanotime		= self.data['gpsnanotime']
        self.augerId		= self.data['id']
        self.sdId		= self.data['sdid']
        self.code   		= self.data['sdrec']['code']
        self.version   	= self.data['sdrec']['version']
        self.theta    		= self.data['sdrec']['theta']
        self.phi		= self.data['sdrec']['phi']
        self.energy		= self.data['sdrec']['energy']
        self.dtheta   		= self.data['sdrec']['dtheta']
        self.dphi     		= self.data['sdrec']['dphi']
        self.denergy  		= self.data['sdrec']['denergy']
        self.l   		= self.data['sdrec']['l']
        self.b   		= self.data['sdrec']['b']
        self.ra  		= self.data['sdrec']['ra']
        self.dec 		= self.data['sdrec']['dec']
        self.x			= self.data['sdrec']['x']
        self.y			= self.data['sdrec']['y']
        self.z 		= self.data['sdrec']['z']
        self.dx  		= self.data['sdrec']['dx']
        self.dy		= self.data['sdrec']['dy']
        self.easting 		= self.data['sdrec']['easting']
        self.northing  		= self.data['sdrec']['northing']
        self.altitude		= self.data['sdrec']['altitude']
        self.r   		= self.data['sdrec']['r']
        self.dr  		= self.data['sdrec']['dr']
        self.s1000		= self.data['sdrec']['s1000']
#        self.ldftype 		= self.data['sdrec']['ldftype']
        self.beta   		= self.data['sdrec']['beta']
        self.gamma  		= self.data['sdrec']['gamma']
        self.chi2   		= self.data['sdrec']['chi2']
        self.ndf   		= self.data['sdrec']['ndf']
        self.nbstat		= self.data['sdrec']['nbstat']
        self.recstations	= self.data['sdrec']['recstations']
        self.stations		= wcd.station(self.data['stations']).get_stations()

  
    def plotLDF(self):
    	Wcd=self.stations
    	signal=self.stations.signal
    	candidates=self.stations.iscandidate
    	s1000=self.s1000
    	beta=self.beta
    	gamma=self.gamma
    	distance=self.stations.spdistance
    	
    	dist=[]
    	sig=[]
    	for i , wcd in enumerate(candidates):
    	    if(wcd==1):

    	        
    	        dist.append(distance[i]   )
    	        sig.append(signal[i])
    	       
    	fit=[]
    	d=[]
    	for i in np.linspace(np.asarray(dist).min()-100,np.asarray(dist).max(),100):
        	d.append(i)
        	fit.append(s1000*pow(i/1000,beta)*pow((i+700)/(1000+700),beta+gamma ))
    	plt.scatter(dist,sig,label="candidate station")
    	plt.plot(d,fit,'r',label="fit")
    	plt.xlabel("Distance from the core [m]")
    	plt.ylabel("Signal [VEM]")
    	plt.legend()
    	plt.show()

  
    	return

 
    def plotStation(self,idstat,figsize=[10,5],xlim=[0,20], pmt=0):
    	selId=self.stations.loc[self.stations['id']==idstat].index[0]
    	selWcd=self.stations.iloc[selId]
	

    	pmt1=np.asarray(selWcd.pmt1)
    	pmt2=np.asarray(selWcd.pmt2)
    	pmt3=np.asarray(selWcd.pmt3)
	
    	flagPmt1=0
    	flagPmt2=0
    	flagPmt3=0
	
    	if(pmt==0 or pmt==1):
    	  flagPmt1=1
    	if(pmt==0 or pmt==2):
    	  flagPmt2=1
    	if(pmt==0 or pmt==3):
    	  flagPmt3=1				
    	time1=[]
    	time2=[]
    	time3=[]
    	for i in range(len(pmt1)):
    		time1.append(i*25/1000)
    	for i in range(len(pmt2)):
    		time2.append(i*25/1000)
    	for i in range(len(pmt3)):
    		time3.append(i*25/1000)    		

    	fig,ax=plt.subplots(1,1, figsize=(figsize[0], figsize[1]))
    	title='Event Id:'+ self.augerId  + '\nStation Id: '+selWcd.id.astype(str)
    	if(len(pmt1)>0 and flagPmt1==1):
    	        ax.plot(time1,pmt1,'r',label='pmt1')
    	if(len(pmt2)>0 and flagPmt2==1):
        		ax.plot(time2,pmt2,'b',label='pmt2')
    	if(len(pmt3)>0 and flagPmt3==1):
        		ax.plot(time3,pmt3,'g',label='pmt3')

    	ax.set_ylabel('Signal [VEM]')
    	ax.set_xlabel('Time [us]')
    	ax.set_xlim(xlim)
    	ax.legend()



    	plt.show()
    	return 



    def plotMap(self,figsize=[8,16], xlim=[-40,40],ylim=[-40,40]):
    	wcds=self.stations
    	nwcds=len(wcds.id)
    	signal=np.log(wcds.signal[wcds.iscandidate==1])
    	size=signal/signal.max()*60+3
    	x=wcds.x[wcds.iscandidate==1]
    	y=wcds.y[wcds.iscandidate==1]
    	x_no=wcds.x[wcds.iscandidate==0]
    	y_no=wcds.y[wcds.iscandidate==0] 	
    	array=auger_map
    	array_y=array.map.y() 	
    	array_x=array.map.x()
    	
    	      
    	fig,ax=plt.subplots(1,1, figsize=(figsize[0], figsize[1]))    	
    	ax.scatter( array_x,array_y,c='grey',s=3)
    	im=ax.scatter( x/1000,y/1000,c=signal, cmap='jet', s=size)
    	ax.scatter( x_no/1000,y_no/1000,c='black',label='not-triggered', s=3)
    	plt.xlim(xlim[0],xlim[1])
    	plt.ylim(ylim[0],ylim[1])
    	plt.xlabel('km')
    	plt.ylabel('km')
    	c=fig.colorbar(im,ax=ax)
    	c.set_label('Log(Signal[VEM])',fontsize=12)
    	plt.legend()
    	plt.show()




    def plot(self,figsize=[20,10]):
    	wcds=self.stations
    	nwcds=len(wcds.id)
    	col=4
    	row=int(np.round(nwcds/4))
    	fig,axs=plt.subplots(row,col, figsize=(figsize[0], figsize[1]))
    	j=0


  	
	
    	time1=[]
    	time2=[]
    	time3=[]
    	
    	for i in range(len(wcds.pmt1[j])):
    		time1.append(i*25/1000)
    	for i in range(len(wcds.pmt2[j])):
    		time2.append(i*25/1000)
    	for i in range(len(wcds.pmt3[j])):
    		time3.append(i*25/1000)    	
    		
    	for i , ax in np.ndenumerate(axs):

    	    if(len(wcds.pmt1[j])>0):
    	        ax.plot(time1,wcds.pmt1[j],'r',label='pmt1')
    	        
    	    if(len(wcds.pmt2[j])>0):
        		ax.plot(time2,wcds.pmt2[j],'b',label='pmt2')
    	    if(len(wcds.pmt3[j])>0):
        		ax.plot(time3,wcds.pmt3[j],'g',label='pmt3')

#    	    ax.set_ylabel('Signal [VEM]')
#    	    ax.set_xlabel('Time [us]')
    	    ax.legend(fontsize='x-small')
    	    title='Id: '+wcds.id[j].astype(str) 
    	    ax.set_title(title,fontsize=8)

    	    j=j+1
    	    if(j==nwcds):
    	     break
    	fig.text(0.5, 0.04, 'Time [us]', ha='center', va='center', fontsize=10)
    	fig.text(0.06, 0.5, 'Signal [VEM]', ha='center', va='center', rotation='vertical',fontsize=10)		

    	plt.subplots_adjust(wspace = .5)
    	plt.subplots_adjust(hspace = .8)
    	plt.show()


    	return 










        
    def get_event(self):
        return self
   
    def __repr__(self):
    	return  "<utils.event.SdEvent> \n"\
    		"gpstime        : gpstime of the event\n"\
    		"gpsnanotime    : gpsnanotime of the event\n"\
    		"augerId        : ID of the event\n"\
    		"sdId           : raw SD Id of the event\n"\
    		"sdrec          : results of reconstruction \n"\
    		"code           : framework used for reconstruction \n"\
    		"version        : framwork version \n"\
    		"theta          : theta angle [deg]\n"\
    		"phi            : phi angle [deg] \n"\
		    "energy         : energy [EeV] \n"\
		    "dtheta         : uncertainty of theta angle [deg] \n"\
    		"dphi           : uncertainty of phi angle [deg]\n"\
	     	"denergy        : uncertainty of energy [EeV]\n"\
		    "l              : galactic longitude [deg]\n"\
	    	"b              : galactic latitude [deg] \n"\
		    "ra             : right ascension [deg]\n"\
		    "dec            : declination [deg]\n"\
		    "x              : core coordinate x [m] (site system)\n"\
		    "y              : core coordinate y [m] (site system)\n"\
		    "z              : core coordinate z [m] (site system)\n"\
		    "dx             : uncertainty of core coordinate x [m] (site system) \n"\
		    "dy             : uncertainty of core coordinate y [m] (site system)\n"\
		    "easting        : core coordinate Easting [m] (UTM system)\n"\
		    "northing       : core coordinate Northing [m] (UTM system) \n"\
		    "altitude       : core coordinate Altitude [m] (UTM system)\n"\
		    "r              : radius of curvature\n"\
		    "dr             : uncertainty of radius of curvature\n"\
		    "s1000          : s1000\n"\
		    "beta           : beta parameter of the LDF fit \n"\
		    "gamma          : gamma parameter of the LDF fit \n"\
		    "chi2           : chi square of the LDF fit\n"\
		    "ndf            : number of degrees of freedom of thr LDF fit\n"\
		    "nbstat         : number of WCDs in DAQ\n"\
		    "recstations    : IDs of the WCDs used for reconstruction\n"\
    	   	"stations       : details of each WCD in DAQ\n"
    		
    		
    		
def mergeZip(listOfZipFile,outfile):

	with open(listOfZipFile,'r')	as f:
		zips = f.read().splitlines() 
	with zipfile.ZipFile(outfile, 'w') as z1:
		for fname in zips:
			zf = zipfile.ZipFile(fname, 'r')

			for n in zf.namelist():
				z1.writestr(n, zf.open(n).read())

	return z1









##################   FD  ##################
#['code', 'version', 'id', 'SdId', 'FlagSpectrum', 'FlagCalib', 'FlagXmax', 'NofEyes', 'Eyes'])
	
class FdEvent:
    def __init__(self, filename):
        self.data = 		filename
        self.augerId		= self.data['id']
        self.sdId		= self.data['sdid']
        self.code   		= self.data['code']
        self.version   	= self.data['version']
        self.FlagSpectrum	= self.data['FlagSpectrum']
        self.FlagXmax		= self.data['FlagXmax']
        self.FlagCalib		= self.data['FlagCalib']
        self.NofEyes		= self.data['NofEyes']
        self.eyes		= fd.eyes(self.data['Eyes']).get_eyes()



    def __repr__(self):
    	return  "<utils.event.FdEvent> \n"\
    		"augerId                : ID of the event\n"\
    		"sdId                   : raw SD Id of the event\n"\
    		"code                   : framework used for reconstruction \n"\
    		"version                : framwork version \n"\
    		"FlagSpectrum           : is the event used in the spectrum analysis? \n"\
    		"FlagXmax               : is the event used in the xmax analysis? \n"\
    		"FlagCalib              : is the event used in the calibration analysis? \n"\
    		"NofEyes                : number of eyes in DAQ \n"\
    		"eyes                   : details of each eye in DAQ \n"\
    		"eyes.eyeId             : ID of the FD site  \n"\
    		"eyes.eyeName           : Name of the FD site  \n"\
        	"eyes.gpstime           : gpstime of the event\n"\
        	"eyes.gpsnanotime       : gpsnanotime of the event\n"\
        	"eyes.theta             : theta angle of the shower [deg]\n"\
        	"eyes. phi              : phi angle of the shower [deg] \n"\
	    	"eyes.dtheta            : uncertainty of theta angle [deg] \n"\
        	"eyes.dphi              : uncertainty of phi angle [deg]\n"\
	        "eyes.l                 : galactic longitude [deg]\n"\
	        "eyes.b                 : galactic latitude [deg] \n"\
	        "eyes.ra                : right ascension [deg]\n"\
	        "eyes.dec               : declination [deg]\n"\
	    	"eyes.TotalEnergy       : total energy [EeV] \n"\
	    	"eyes.TotalEnergyErr    : uncertainty of total energy [EeV] \n"\
	    	"eyes.CalEnergy         : calorimetric energy [EeV] \n"\
	    	"eyes.CalEnergyErr      : uncertainty of calorimetric energy [EeV] \n"\
            "eyes.Xmax              : slant depth of shower maximum [g/cm^2] \n"\
            "eyes.XmaxErr           : uncertainty slant depth of shower maximum [g/cm^2] \n"\
            "eyes.HeightXmax        : height of shower maximum ( [m] a.s.l. ) \n"\
            "eyes.DistXmax          : distance of shower maximum from eye [m]\n"\
            "eyes.dEdXmax           : energy deposit at maximum [PeV/(g/cm^2)] \n"\
            "eyes.dEdXmaxError      : uncertainty of energy deposit at maximum [PeV/(g/cm^2)] \n"\
	        "eyes.x                 : core coordinate x [m] (site system)\n"\
	        "eyes.y                 : core coordinate y [m] (site system)\n"\
	        "eyes.z                 : core coordinate z [m] (site system)\n"\
	        "eyes.dx                : uncertainty of core coordinate x [m] (site system) \n"\
	        "eyes.dy                : uncertainty of core coordinate y [m] (site system)\n"\
	        "eyes.easting           : core coordinate Easting [m] (UTM system)\n"\
	        "eyes.northing          : core coordinate Northing [m] (UTM system) \n"\
	        "eyes.altitude          : core coordinate Altitude [m] (UTM system)\n"\
	        "eyes.UspL              : Gaisser-Hillas USP L parameter [g/cm2] \n"\
	        "eyes.UspLError         : uncertainty of Gaisser-Hillas USP L parameter [g/cm2] \n"\
	        "eyes.UspR              : Gaisser-Hillas USP R parameter \n"\
	        "eyes.UspRError         : uncertainty of Gaisser-Hillas USP L parameter  \n"\
    		"eyes.HottestStationId  : ID of the hottest station  \n"\
    		"eyes.DistSDPStation    : distance of the hottest station to the SDP [m] \n"\
    		"eyes.DistAXISStation   : distance of the hottest station to the axis in SP coords[m]  \n"\
    		"eyes.CherenkovFraction : fraction of Cherenkov light at diaphragm \n"\
    		"eyes.minViewAngle      : minimum angle eye-track [deg] \n"\
    		"eyes.FlagSpectrumEye   : is the eye used in the spectrum analysis? \n"\
    		"eyes.FlagXmaxEye       : is the eye used in the xmax analysis? \n"\
    		"eyes.FlagCalibEye      : is the eye used in the calibration analysis? \n"\

    
    		
    		
    		

    def plotProfiles(self, figsize=[10,5],xlim=[100,1400]): 
    
    	eyes=self.eyes
    	name=eyes.eyeName
    	eye=eyes.eyeId
    	atmDepth=eyes.atmDepthProf
    	depEn=eyes.energyDepositProf
    	depEnErr=eyes.energyDepositProfErr
    	L=eyes.UspL
    	R=eyes.UspR
    	Xmax=eyes.Xmax
    	dEdXmax=eyes.dEdXmax

    	

    	fig,axs=plt.subplots(len(eye),1, figsize=(figsize[0], figsize[1]))

    	vec_fit=[]
    	for i,atm in enumerate(atmDepth):

    	    fit=[]
    	    for x in range(50,1400,20):

    	        fit.append( dEdXmax[i]*      pow( 1+R[i]*(x-Xmax[i])/L[i],pow(R[i],-2))*np.exp(-(x-Xmax[i])/(R[i]*L[i]))            )
    	        
    	    vec_fit.append(fit)

    	if(len(eye)>1):
    	    for i in range(len(eye)):
    	#    	axs[i].set_title(name[i])
    	    	axs[i].set_title('Eye ' + str(eye[i]), fontsize=16)
    	    	axs[i].set_xlim([xlim[0],xlim[1]])
    	   # 	axs[i].set_ylim([ylim[0],ylim[1]])
    	    	axs[i].errorbar(atmDepth[i],depEn[i],yerr=depEnErr[i],linestyle='',marker='o', alpha=0.5, markersize=3, linewidth=1,label='data')
    	    	axs[i].plot(range(50,1400,20),vec_fit[i],label='GH fit')
    	    	#if(i==len(eye)-1):
    	    	axs[i].set_xlabel('Slant depth $[g/cm^2]$' )
    	    	axs[i].set_ylabel('dE/dX $[PeV/(g/cm^2)]$')
    	    	axs[i].legend()
    	    	plt.subplots_adjust(hspace = 0.5)
    	elif(len(eye)==1):

    		axs.set_xlim([100,1400])
    		axs.set_title('Eye ' + str(eye[0]), fontsize=16)

 
    		axs.errorbar(atmDepth[0],depEn[0],yerr=depEnErr[0],linestyle='',marker='o', alpha=0.5, markersize=3, linewidth=1,label='data')
    		axs.plot(range(50,1400,20),vec_fit[0],label='GH fit')

    		axs.set_xlabel('X $[g/cm^2]$' )
    		axs.set_ylabel('dE/dX $[PeV/(g/cm^2)]$')
    		plt.subplots_adjust(hspace = 0.7)




    		
    	plt.show()


    	
    def plot(self,figsize=[30,5]): 
     	eyes=self.eyes
     	name=eyes.eyeName
     	eye=eyes.eyeId
     	pixelID=eyes.pixelID
     	pixelTime=eyes.pixelTime
     	pixelCharge=eyes.pixelCharge
     	pixelStatus=eyes.pixelStatus



     	for n in range(len(eye)):

     	     tel_phi=[]
     	     tel_elev=[]
     	     for tel in range(6):
                phi=[]
                elev=[]
                for pixel in range(tel*440,(tel+1)*440,1):
                    row=((pixel)%22+1)
                    if(row%2==0):
                        col=(int((pixel)/22)+1)-0.5
                    else:
                        col=(int((pixel)/22)+1)
                    phi.append(-3/2*col+30+tel*60)
                    elev.append(row*1.36)
                tel_elev=elev
                tel_phi.append(phi)
                    

     	     pixelphi=[]
     	     pixelelev=[]
     	     chargecolor=[]
     	     timecolor=[]
     	     bad_phi=[]
     	     bad_el=[]
                

     	     i=0
     	     for pixel in pixelID[n]:
                 if(pixelStatus[n][i]!=4):
                    tel=int(pixel/440%6)
                    row=pixel%22+1
                    if(row%2==0):
                        col=(int((pixel)/22)+1)-0.5
                    else:
                        col=(int((pixel)/22)+1)

                    bad_phi.append(-3/2*col+30+tel*60)
                    bad_el.append(row*1.36)

                    i=i+1
                    continue
                 else:
                    tel=int(pixel/440%6)
                    row=pixel%22+1
                    if(row%2==0):
                        col=(int((pixel)/22)+1)-0.5
                    else:
                        col=(int((pixel)/22)+1)

                    pixelphi.append(-3/2*col+30+tel*60)
                    pixelelev.append(row*1.36)
                    chargecolor.append(pixelCharge[n][i])
                    timecolor.append(pixelTime[n][i])
                    
                    i=i+1




     	     fig,axs = plt.subplots(2,6, figsize=(figsize[0], figsize[1]))
     	     fig.suptitle('Eye'+ str(eye[n]) +': Pixel Traces', fontsize=20)
     	     im_ch=[]
     	     im_time=[]
     	     for tel in range(len(tel_phi)):     	     
                 axs[0,5-tel].scatter(tel_phi[tel],tel_elev,marker='H',s=50, c='grey', alpha=0.2)  
                 im_ch.append(axs[0,5-tel].scatter(pixelphi,pixelelev, c=chargecolor,marker='h',s=50))
                 axs[0,5-tel].scatter(bad_phi,bad_el, c='grey',marker='h',s=50)
                 if(tel==0 or tel==5):
                     axs[0,5-tel].set_title("tel"+str(tel+1),fontsize=16)
                 axs[0,5-tel].set_xlim([30*(tel+1)-0.75,30*tel-1.5])
                 axs[0,5-tel].set_xticks(np.arange(30*(tel+1)-10, 30*tel-1, step=-10))
                 plt.subplots_adjust(wspace = .01)

     	     
                 if(tel!=5):
                   axs[0,5-tel].set_yticks([])
                 if(tel==5):
                   axs[0,5-tel].set_xticks(np.arange(30*(tel+1), 30*tel-1, step=-10))
                 axs[0,5-tel].tick_params(labelsize=14)
            

                 axs[1,5-tel].scatter(tel_phi[tel],tel_elev,marker='H',s=50, c='grey', alpha=0.2)  
                 im_time.append(axs[1,5-tel].scatter(pixelphi,pixelelev, c=timecolor,marker='h',s=50))
                 axs[1,5-tel].scatter(bad_phi,bad_el, c='grey',marker='h',s=50)
                 axs[1,5-tel].set_xlim([30*(tel+1)-0.75,30*tel-1.5])
                 axs[1,5-tel].set_xticks(np.arange(30*(tel+1)-10, 30*tel-1, step=-10))
                 plt.subplots_adjust(wspace = .01)

     	     
                 if(tel!=5):
                   axs[1,5-tel].set_yticks([])
                 if(tel==5):
                   axs[1,5-tel].set_xticks(np.arange(30*(tel+1), 30*tel-1, step=-10))
                 axs[1,5-tel].tick_params(labelsize=14)


                 
                 fig.text(0.5, 0.04, 'azimuth [deg]', ha='center', va='center', fontsize=16)
                 fig.text(0.06, 0.5, 'elevation [deg]', ha='center', va='center', rotation='vertical',fontsize=16)
                 axlist_c=[axs[0,0],axs[0,1],axs[0,2],axs[0,3],axs[0,4],axs[0,5]]
                 axlist_t=[axs[1,0],axs[1,1],axs[1,2],axs[1,3],axs[1,4],axs[1,5]]
     	
#     	plt.colorbar(axs[0])
                 cbar_c=fig.colorbar(im_ch[0], ax=axlist_c,location='right',fraction=0.02, pad=0.02)
                 cbar_t=fig.colorbar(im_time[0], ax=axlist_t,location='right',fraction=0.02, pad=0.02)
                 cbar_c.set_label('photons at aperture',fontsize=12)
                 cbar_t.set_label('time [100 ns]',fontsize=12)
     	plt.show()





class FdEventCollection:
	def __init__(self, file_name,n_ev=0, ev_id=None):
		data_json=[]
		self.list=[]
		self.flag=ev_id
		if (ev_id==None):
			with zipfile.ZipFile(file_name,'r') as z:
				if(n_ev==0):
					n_ev=len(z.namelist())
				j=0
				for filename in z.namelist():
					if (j<n_ev):		
						self.list.append(filename)
						with z.open(filename) as f:
							data = f.read()
						data_json.append(data)
					j=j+1
			self.listFD=FdColl()


			for i in range(len(data_json)):	
				event=FdEvent(json.loads(data_json[i]))
				self.listFD.add(event)
			
		else:
			with zipfile.ZipFile(file_name,'r') as z:
				for filename in z.namelist():
					if(filename==ev_id):
						self.list.append(filename)
						with z.open(filename) as f:
							data = f.read()
						data_json.append(data)
			self.listFD=FdColl()
			for i in range(len(data_json)):	
				event=FdEvent(json.loads(data_json[i]))
				self.listFD.add(event)	
		
		self.varList=FDVariablesColl()
		for event in self.listFD:
		    for i in range(event.NofEyes):
		    	self.varList.augerId.append(event.augerId)
		    	self.varList.sdId.append(event.sdId)
		    	if(event.code=='Offline'):
			        code_index=1
		    	else:
			        code_index=0
		    	self.varList.code.append(code_index)
		    	self.varList.version.append(event.version)
		    	self.varList.FlagSpectrum.append(event.FlagSpectrum)
		    	self.varList.FlagCalib.append(event.FlagCalib)
		    	self.varList.FlagXmax.append(event.FlagXmax)		    	
		    	self.varList.eyeId.append(event.eyes.eyeId[i])		    			  
		    	self.varList.eyeName.append(event.eyes.eyeName[i])
		    	self.varList.gpstime.append(event.eyes.gpstime[i])
		    	self.varList.gpsnanotime.append(event.eyes.gpsnanotime[i])
		    	self.varList.theta.append(event.eyes.theta[i])
		    	self.varList.thetaErr.append(event.eyes.thetaErr[i])
		    	self.varList.phi.append(event.eyes.phi[i])
		    	self.varList.phiErr.append(event.eyes.phiErr[i])
		    	self.varList.TotalEnergy.append(event.eyes.TotalEnergy[i])
		    	self.varList.TotalEnergyErr.append(event.eyes.TotalEnergyErr[i])
		    	self.varList.CalEnergy.append(event.eyes.CalEnergy[i])
		    	self.varList.CalEnergyErr.append(event.eyes.CalEnergyErr[i])
		    	self.varList.Xmax.append(event.eyes.Xmax[i])
		    	self.varList.XmaxErr.append(event.eyes.XmaxErr[i])
		    	self.varList.HeightXmax.append(event.eyes.HeightXmax[i])
		    	self.varList.DistXmax.append(event.eyes.DistXmax[i])
		    	self.varList.dEdXmax.append(event.eyes.dEdXmax[i])
		    	self.varList.dEdXmaxError.append(event.eyes.dEdXmaxError[i])
		    	self.varList.UspL.append(event.eyes.UspL[i])
		    	self.varList.UspLError.append(event.eyes.UspLError[i])
		    	self.varList.UspR.append(event.eyes.UspR[i])
		    	self.varList.UspRError.append(event.eyes.UspRError[i])
		    	self.varList.FlagSpectrumEye.append(event.eyes.FlagSpectrumEye[i])
		    	self.varList.FlagCalibEye.append(event.eyes.FlagCalibEye[i])
		    	self.varList.FlagXmaxEye.append(event.eyes.FlagXmaxEye[i])
		    	self.varList.HottestStationId.append(event.eyes.HottestStationId[i])
		    	self.varList.DistSDPStation.append(event.eyes.DistSDPStation[i])
		    	self.varList.DistAXISStation.append(event.eyes.DistAXISStation[i])
    
		    	self.varList.X.append(event.eyes.X[i])           
		    	self.varList.Y.append(event.eyes.Y[i])              
		    	self.varList.Z.append(event.eyes.Z[i])               
		    	self.varList.dX.append(event.eyes.dX[i])              
		    	self.varList.dY.append(event.eyes.dY[i])                 
		    	self.varList.easting.append(event.eyes.easting[i])             
		    	self.varList.northing.append(event.eyes.northing[i])        
		    	self.varList.altitude.append(event.eyes.altitude[i])     
		    	self.varList.CherenkovFraction.append(event.eyes.CherenkovFraction[i])           
		    	self.varList.minViewAngle.append(event.eyes.minViewAngle[i])                
		    	self.varList.l.append(event.eyes.l[i])           
		    	self.varList.b.append(event.eyes.b[i])              
		    	self.varList.ra.append(event.eyes.ra[i])               
		    	self.varList.dec.append(event.eyes.dec[i])


		dict={
		'augerId':self.varList.augerId,
		'sdId':self.varList.sdId,
		'code':self.varList.code,
#		'version':self.varList.version,
		'FlagSpectrum':self.varList.FlagSpectrum,
		'FlagCalib':self.varList.FlagCalib,
		'FlagXmax':self.varList.FlagXmax,
		'eyeId':self.varList.eyeId,
#		'eyeName':self.varList.eyeName,
		'gpstime':self.varList.gpstime,
		'gpsnanotime':self.varList.gpsnanotime,
		'theta':self.varList.theta,
		'thetaErr':self.varList.thetaErr,
		'phi':self.varList.phi,
		'phiErr':self.varList.phiErr,
		'TotalEnergy':self.varList.TotalEnergy,
		'TotalEnergyErr':self.varList.TotalEnergyErr,
		'CalEnergy':self.varList.CalEnergy,
		'CalEnergyErr':self.varList.CalEnergyErr,
		'Xmax':self.varList.Xmax,
		'XmaxErr':self.varList.XmaxErr,
		'HeightXmax':self.varList.HeightXmax,
		'DistXmax':self.varList.DistXmax,
		'dEdXmax':self.varList.dEdXmax,
		'dEdXmaxError':self.varList.dEdXmaxError,
		'UspL':self.varList.UspL,
		'UspLError':self.varList.UspLError,
		'UspR':self.varList.UspR,
		'UspRError':self.varList.UspRError,
		'FlagSpectrumEye':self.varList.FlagSpectrumEye,
		'FlagCalibEye':self.varList.FlagCalibEye,
		'FlagXmaxEye':self.varList.FlagXmaxEye,
		'HottestStationId':self.varList.HottestStationId,
		'DistSDPStation':self.varList.DistSDPStation,
		'DistAXISStation':self.varList.DistAXISStation,
		'X':self.varList.X,
		'Y':self.varList.Y,
		'Z':self.varList.Z,
		'dX':self.varList.dX,
		'dY':self.varList.dY,
		'easting':self.varList.easting,
		'northing':self.varList.northing,
		'altitude':self.varList.altitude,
		'CherenkovFraction':self.varList.CherenkovFraction, 
		'minViewAngle':self.varList.minViewAngle,
		'l':self.varList.l,
		'b':self.varList.b,
		'ra':self.varList.ra,
		'dec':self.varList.dec}

		self.df=pd.DataFrame(dict)
	
   	
	def get_df(self):
   		return self.df
		
	def list(self):
		return self.list
		
	def get_collection(self):
		return self.listFD

	def get_event(self):
		if(self.flag==None):
			print("Warning: get_event() works only for single event selection")
			return 
		else:
			return self.listFD[0]
	def get_varList(self):
		return self.varList
	
	def __repr__(self):
   		return  "List of events in the zip file: View the list using FdEvent.list; access i^th element using FdEvent.list[i] \n"

   		



class FdColl(list):
	def __init__(self):
		self =[]
	
	def add(self, event):
		self.append(event)
		
	def get(self):
		return self

	def __repr__(self):
		return "Collection of FdEvent\n"




class FDVariablesColl():
	def __init__(self):
		self.augerId 		= []
		self.sdId   		= []
		self.code    		= []
		self.version   	= []
		self.FlagSpectrum	= []
		self.FlagXmax		= []
		self.FlagCalib		= [] 
		self.NofEyes   	= []
		self.eyeId     	= []
		self.eyeName  		= []
		self.gpstime  		= []
		self.gpsnanotime  		= []
		self.theta   		= []
		self.thetaErr  	= []
		self.phi 		= []
		self.phiErr		= []
		self.TotalEnergy	= []
		self.TotalEnergyErr	= []
		self.CalEnergy  	= []
		self.CalEnergyErr	= []
		self.Xmax   		= []
		self.XmaxErr  		= []
		self.HeightXmax	= []
		self.DistXmax 		= []
		self.dEdXmax   	= []
		self.dEdXmaxError	= []
		self.UspL   		= []
		self.UspLError 	= []
		self.UspR		= []
		self.UspRError		= []
		self.FlagSpectrumEye	= []
		self.FlagCalibEye	= []
		self.FlagXmaxEye	= []
		self.HottestStationId	= []
		self.DistSDPStation	= []
		self.DistAXISStation	= []
		self.X  = []              
		self.Y  = []              
		self.Z  = []               
		self.dX  = []              
		self.dY  = []                 
		self.easting  = []             
		self.northing  = []            
		self.altitude  = []               
		self.CherenkovFraction  = []            
		self.minViewAngle  = [] 
		self.l  = []              
		self.b  = []              
		self.ra  = []               
		self.dec  = []   
		

########

def readJson(filename):
	with open(filename) as f:
		data = f.read()
	event=json.loads(data)
	try:
         return SdEvent(event)
	except:
		 return FdEvent(event)

