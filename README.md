Code to read the json files compressed in zip, it can be useful to check the set of data

The folder 'data' contains some files to test the code

Download the code.
- Open a python shell (tested with ipython3)
- import the custom module 'event' from the directory 'utils' -> import utils.event as evn
- import pyplot -> import matplotlib.pyplot as plt

The goal of the code is to convert  json files in  objects called 'SdEvent' and 'FdEvent'.
An SdEvent (FdEvent) object contains as attributes all the variables contained in the json

You can read the files in different ways:
#case 1: 
#read a single json file (txt format) and convert it in an SdEvent (FdEvent) object
event=evn.readJson('data/SD/70767063900') # SD
event=evn.readJson('data/FD/70544999800') #FD

#case 2:
#read a zip file containing many json files, and convert all the json files in a collection of SdEvent (FdEvent) object
eventColl_all = evn.SdEventCollection("data/SD/SD.zip") #SdEventCollection
eventColl_all = evn.FdEventCollection("data/FD/FD.zip") #FdEventCollection

#case 3:
#read a zip file containing many json files, and convert the firsts n=2 json files in a collection of SdEvent (FdEvent) object. n=0=all files
eventColl_n = evn.SdEventCollection("data/SD/SD.zip",2)
eventColl_n = evn.SdEventCollection("data/FD/FD.zip",2)

#case 4:
#read a zip file containing many json files, and convert  a specific json files in an SdEvent (FdEvent) object.
eventColl_sel = evn.SdEventCollection("data/SD.zip",0,'2004/040017088400')

#merge different zipfile in one:
#first write a list of the zipfiles in txt file (you need to write the entire path)--> ls *.zip > $PWD/data/listzip.txt (not in pythoh shell)
#
evn.mergeZip('data/FD/ziplist.txt','data/merged.zip')
eventColl_merged = evn.FdEventCollection("data/merged.zip")



## handle a collection of SdEvent (FdEvent):

#get a dataframe containing all the info of each station (eye)
df=eventColl_all.get_df()

#list all the file in the collection
eventColl_all.list

#read the collection
events=eventColl_all.get_collection()

#select a single event of the collection
my_event=events[1]



##method to plot the pmts signal of an SdEvent
my_event.plot()  

#to plot signals of a single station use the function: plotStation(id) , where id is the id of the station
my_event.plotStation(751)


#create an object that collect the attributes of each SdEvent (FdEvent) in lists of attrubute
varlist=eventColl_all.get_varList()

#each list of attributes can be easily plotted
plt.hist(varlist.theta)   
plt.show()

plt.plot(varlist.phi)
plt.show()

plt.plot(varlist.phi,varlist.theta,'o')
plt.show()

